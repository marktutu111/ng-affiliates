import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Component({
    selector: 'app-leadform',
    templateUrl: 'leadform.component.html'
})

export class LeadFormComponent implements OnInit {

    @Output() onData:EventEmitter<any>=new EventEmitter();
    @Input() set data(val:any){this.formGroup.patchValue(val)};

    formGroup:FormGroup;
    private subscription$=new Subject();

    constructor() {
        this.formGroup=new FormGroup(
            {
                createdBy:new FormControl('',Validators.required),
                name:new FormControl('',Validators.required),
                email:new FormControl('',Validators.required),
                description:new FormControl('',Validators.required),
                location:new FormControl('',Validators.required),
                businessType:new FormControl('',Validators.required),
                cnumber:new FormControl('',Validators.required),
                cperson:new FormControl('',Validators.required),
            }
        )
    }

    ngOnInit() {
        this.formGroup.valueChanges
            .pipe(takeUntil(this.subscription$))
            .subscribe(()=>{
                this.onData.emit(this.formGroup)
            })
    }

    ngOnDestroy(): void {
        this.subscription$.next();
        this.subscription$.complete();
    }


}