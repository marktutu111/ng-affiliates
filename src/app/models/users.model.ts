export interface IUser {
    _id:string;
    name:string;
    email:string;
    phone:string;
    password:String,
    active:boolean;
    blocked:boolean;
    role:String,
    createdAt:string;
    updatedAt:string;
}