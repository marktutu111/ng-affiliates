import { AffiliatesInterface } from "./affiliates.model";


export enum LeadTypes {
    '_id'='ID',
    'createdBy'='Created By',
    'name'='Lead Name',
    'email'='Lead Email',
    'description'='Lead Description',
    'location'='Location',
    'businessType'='Bussiness Type',
    'cnumber'='Contact Number',
    'cperson'='Contact Person',
    'stage'='Funnel Stage',
    'notes'='Notes',
    'createdAt'='Date Created',
    'updatedAt'='Date Updated' 
}

export interface LeadInterface {
    _id:string;
    createdBy:AffiliatesInterface;
    name:string;
    email:string;
    description:string;
    location:string;
    businessType:string;
    assigned:string[];
    cnumber:string;
    cperson:string;
    stage:string;
    notes:string;
    createdAt?:string;
    updatedAt?:string;
}


export class LeadModel implements LeadInterface {

    _id:string;
    createdBy:AffiliatesInterface;
    name:string;
    email:string;
    description:string;
    location:string;
    businessType:string;
    cnumber:string;
    cperson:string;
    assigned:string[];
    createdAt?:string;
    updatedAt?:string;
    stage: string;
    notes: string;

    constructor(lead:LeadInterface){
        this._id=lead._id;
        this.createdBy=lead.createdBy;
        this.name=lead.name;
        this.email=lead.email;
        this.description=lead.description;
        this.location=lead.location;
        this.businessType=lead.businessType;
        this.cnumber=lead.cnumber;
        this.cperson=lead.cperson;
        this.notes=lead.notes;
        this.stage=lead.stage;
        this.assigned=lead.assigned;
        this.createdAt=lead.createdAt;
        this.updatedAt=lead.updatedAt;
    };

    fullname():string{
        return `${this.createdBy.fname} ${this.createdBy.lname}`
    }


}