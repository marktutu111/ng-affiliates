
export interface AffiliatesInterface {
    _id?:string;
    fname:string;
    lname:string;
    phone:string;
    email:string;
    profession:string;
    organization:string;
    city:string;
    region:string;
    gender:string;
    blocked?:boolean;
    createdAt?:string;
    updatedAt?:string;
}