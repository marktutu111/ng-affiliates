
export interface StagesInterface {
    _id?:string;
    name:string;
    description:string;
    createdAt?:string;
    updatedAt?:string;
}