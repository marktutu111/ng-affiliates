
export interface AccountsInterface {
    _id?:string;
    name:string;
    description:string;
    createdAt?:string;
    updatedAt?:string;
}