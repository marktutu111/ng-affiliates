import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AffiliatesInterface } from '../../models/affiliates.model';
import { AffiliateService } from '../../services/affiliates.service';
import { AppState } from '../../states/app.state';

@Component({
    selector: 'app-affiliates',
    templateUrl: 'affiliates.component.html'
})

export class AffiliatesComponent implements OnInit {

    loading:string='';
    private subscription$=new Subject();

    constructor(public state:AppState,private service:AffiliateService) {};

    ngOnInit() {
        this.loadAffs();
    };

    loadAffs():void{
        this.service.getall();
    }

    block(aff:AffiliatesInterface):void{
        this.loading=aff._id;
        this.service.update({id:aff._id,data:{blocked:!aff.blocked}}).pipe(takeUntil(this.subscription$))
            .subscribe((response:any)=>{
                this.loading='';
                alert(response.message);
                this.loadAffs();
            })
    }


}