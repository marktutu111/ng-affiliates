import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { IUser } from '../../models/users.model';
import { UserService } from '../../services/users.service';
import { AppState } from '../../states/app.state';

@Component({
    selector: 'app-users',
    templateUrl: 'users.component.html'
})

export class UsersComponent implements OnInit {

    loading:string='';
    private subscription$=new Subject();
    constructor(public state:AppState,private service:UserService) {};

    ngOnInit() {
        this.service.loadUsers();
    };

    blockUser(user:IUser):void{
        this.loading=user._id;
        const data={id:user._id,data:{blocked:!user.blocked}};
        this.service.update(data).pipe(takeUntil(this.subscription$)).subscribe((response:any)=>{
            this.loading='';
            alert(response.message);
            this.service.loadUsers();
        })
    }

    ngOnDestroy(): void {
        this.subscription$.next();
        this.subscription$.complete();
    }


}