import { NgModule } from '@angular/core';
import { NbMenuModule } from '@nebular/theme';

import { ThemeModule } from '../@theme/theme.module';
import { PagesComponent } from './pages.component';
import { RouterModule, Routes } from '@angular/router';
import { LeadsComponent } from './leads/leads.component';
import { AffiliatesComponent } from './affiliates/affiliates.component';
import { AffiliateDetailsComponent } from './affiliate-details/affiliate-details.component';
import { CreateLeadComponent } from './create-lead/create-lead.component';
import { LeadService } from '../services/leads.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { LeadDetailsComponent } from './lead-details/lead-details.component';
import { SettingsComponent } from './settings/settings.component';
import { StageService } from '../services/stages.service';
import { AccountService } from '../services/accounts.service';
import { UserService } from '../services/users.service';
import { UsersComponent } from './users/users.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { CommonModule } from '@angular/common';
import { LeadFormComponent } from '../@theme/components/leadform/leadform.component';
import { UpdateLeadComponent } from './update-lead/update-lead.component';

const routes: Routes = [{
  path: '',
  component: PagesComponent,
  children: [
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full',
    },
    {
      path: 'dashboard',
      component:DashboardComponent
    },
    {
      path:'leads',
      component:LeadsComponent
    },
    {
      path:'leads/update',
      component:UpdateLeadComponent
    },
    {
      path:'leads/new',
      component:CreateLeadComponent
    },
    {
      path:'leads/details',
      component:LeadDetailsComponent
    },
    {
      path:'affiliates',
      component:AffiliatesComponent
    },
    {
      path:'affiliates/details',
      component:AffiliateDetailsComponent
    },
    {
      path:'settings',
      component:SettingsComponent
    },
    {
      path:'users',
      component:UsersComponent
    },
    {
      path: '**',
      redirectTo:''
    },
  ],
}];

@NgModule({
  declarations: [
    PagesComponent,
    LeadsComponent,
    AffiliatesComponent,
    AffiliateDetailsComponent,
    CreateLeadComponent,
    LeadDetailsComponent,
    SettingsComponent,
    UsersComponent,
    DashboardComponent,
    LeadFormComponent,
    UpdateLeadComponent
  ],
  imports: [
    CommonModule,
    ThemeModule,
    NbMenuModule,
    ReactiveFormsModule,
    FormsModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
    RouterModule.forChild(routes)
  ],
  providers:[
    LeadService,
    StageService,
    AccountService,
    UserService
  ]
})
export class PagesModule {}
