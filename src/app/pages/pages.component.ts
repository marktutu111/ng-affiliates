import { Component, OnDestroy, OnInit } from '@angular/core';
import { NbSearchService } from '@nebular/theme';
import { Subject, Subscription } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AuthTypes } from '../models/common.model';
import { AppState } from '../states/app.state';

import { MENU_ITEMS, MENU_ITEMS_AFF } from './pages-menu';

@Component({
  selector: 'ngx-pages',
  styleUrls: ['pages.component.scss'],
  template: `
    <ngx-one-column-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-one-column-layout>
  `,
})
export class PagesComponent implements OnInit,OnDestroy {

    private subscription$=new Subject();
    menu=[];
    constructor(private search$:NbSearchService,private state:AppState){};

    
    ngOnInit(): void {
      this.search$.onSearchSubmit().pipe(takeUntil(this.subscription$)).subscribe((search)=>{this.state.onSearch(search.term || '')});
      this.state.auth$.pipe(takeUntil(this.subscription$))
        .subscribe(({type})=>{
          switch (type) {
            case AuthTypes.ADMIN:
              this.menu=MENU_ITEMS;
              break;
            case AuthTypes.AFFILIATE:
              this.menu=MENU_ITEMS_AFF;
              break;
            default:
              break;
          }
        })
      
    }


    ngOnDestroy(): void {
      this.subscription$.next();
      this.subscription$.complete();
    }


}
