import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { LeadService } from '../../services/leads.service';
import { AppState } from '../../states/app.state';


@Component({
    selector: 'update-lead',
    templateUrl: 'update-lead.component.html'
})

export class UpdateLeadComponent implements OnInit {


    loading:boolean=false;
    form:FormGroup;
    private subscription$=new Subject();

    constructor(private service:LeadService,public state:AppState) {};

    ngOnInit() {};

    onChange(form:FormGroup):void{
        this.form=form;
    }

    update():void{
        if(!this.form){
            return alert(
                'No change has been made'
            )
        }
        this.loading=true;
        const {_id}=this.state.selected$.value;
        this.service.updateLead({id:_id,data:this.form.value})
            .pipe(takeUntil(this.subscription$))
            .subscribe((response:any)=>{
                this.loading=false;
                alert(response.message);
            })
    }

    ngOnDestroy(): void {
        this.subscription$.next();
        this.subscription$.complete();
    }

}