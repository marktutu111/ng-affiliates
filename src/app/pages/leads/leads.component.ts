import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthTypes } from '../../models/common.model';
import { LeadService } from '../../services/leads.service';
import { AppState } from '../../states/app.state';

@Component({
    selector: 'app-leads',
    templateUrl: 'leads.component.html'
})

export class LeadsComponent implements OnInit {


    constructor(public state:AppState,private service:LeadService,private router:Router) {};

    ngOnInit() {
        this.loadLeads();
    };


    loadLeads():void{
        this.service.loadLeads();
    }


    view(data:any):void{
        this.state.setSelected(data);
        this.router.navigate(['home/leads/details']);
    }



}