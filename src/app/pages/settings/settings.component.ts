import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AffiliatesInterface } from '../../models/affiliates.model';
import { AuthTypes, SettingsEnum, SettingsType } from '../../models/common.model';
import { IUser } from '../../models/users.model';
import { AccountService } from '../../services/accounts.service';
import { AffiliateService } from '../../services/affiliates.service';
import { StageService } from '../../services/stages.service';
import { UserService } from '../../services/users.service';
import { AppState } from '../../states/app.state';

@Component({
    selector: 'app-settings',
    templateUrl: 'settings.component.html'
})

export class SettingsComponent implements OnInit {

    authEnum=AuthTypes;
    settingsEnum=SettingsEnum;
    loading:SettingsType='';
    formGroup:FormGroup;
    private subscription$=new Subject();
    private user:IUser|AffiliatesInterface=null;

    constructor(private fb:FormBuilder,private stageService:StageService,
        public state:AppState,private accountService:AccountService,
        private userService:UserService,private afService:AffiliateService) {
        this.formGroup=this.fb.group(
            {
                stages:new FormGroup(
                    {
                        name:new FormControl('',Validators.required),
                        description:new FormControl('',Validators.required)
                    }
                ),
                accounts:new FormGroup(
                    {
                        name:new FormControl('',Validators.required),
                        description:new FormControl('',Validators.required)
                    }
                ),
                user:new FormGroup(
                    {
                        name:new FormControl('',Validators.required),
                        email:new FormControl('',[Validators.email,Validators.required]),
                        phone:new FormControl('',Validators.required),
                        password:new FormControl('',Validators.required),
                        role:new FormControl('',Validators.required)
                    }
                ),
                password:new FormGroup(
                    {
                        oldpassword:new FormControl('',Validators.required),
                        newpassword:new FormControl('',Validators.required)
                    }
                )
            }
        )
    }


    ngOnInit() {
        this.user=this.state.auth$.value.user;
        this.stageService.getstages();
        this.accountService.accounts();
    }

    removeStage(id:any):void {
        this.loading=id;
        this.stageService.remove(id)
            .pipe(takeUntil(this.subscription$))
            .subscribe((response:any)=>{
                this.loading='';
                alert(response.message);
                this.stageService.getstages();
            })
    }

    updateAfPassword():void{
        const form=this.formGroup.get('password');
        if(!form.valid)return;
        this.loading=SettingsEnum.AFPASSWORD;
        const data={id:this.user._id,...form.value};
        this.afService.updatePassword(data)
            .pipe(takeUntil(this.subscription$))
            .subscribe((response:any)=>{
                this.loading='';
                form.reset();
                alert(response.message);
                this.state.logout();
            })
    }

    updateAdminPassword():void{
        const form=this.formGroup.get('password');
        if(!form.valid)return;
        this.loading=SettingsEnum.AFPASSWORD;
        const data={id:this.user._id,...form.value};
        this.userService.updatePassword(data)
            .pipe(takeUntil(this.subscription$))
            .subscribe((response:any)=>{
                this.loading='';
                form.reset();
                alert(response.message);
                this.state.logout();
            })
    }


    saveUser():void{
        const form=this.formGroup.get('user');
        if(!form.valid)return;
        this.loading=SettingsEnum.USERS;
        this.userService.adduser(form.value)
            .pipe(takeUntil(this.subscription$))
            .subscribe((response:any)=>{
                this.loading='';
                form.reset();
                alert(response.message);
            })
    }

    removeAccount(id:any):void {
        this.loading=id;
        this.accountService.remove(id)
            .pipe(takeUntil(this.subscription$))
            .subscribe((response:any)=>{
                this.loading='';
                alert(response.message);
                this.accountService.accounts();
            })
    }

    saveAccount():void{
        const form=this.formGroup.get('accounts');
        if(!form.valid)return;
        this.loading=SettingsEnum.ACCOUNTTYPES;
        this.accountService.add(form.value)
            .pipe(takeUntil(this.subscription$))
            .subscribe((response:any)=>{
                this.loading='';
                form.reset();
                alert(response.message);
                this.accountService.accounts();
            })
    }


    saveStage():void{
        const form=this.formGroup.get('stages');
        if(!form.valid)return;
        this.loading=SettingsEnum.STAGES;
        this.stageService.add(form.value)
            .pipe(takeUntil(this.subscription$))
            .subscribe((response:any)=>{
                this.loading='';
                form.reset();
                alert(response.message);
                this.stageService.getstages();
            })
    }



    ngOnDestroy(): void {
        this.subscription$.next();
        this.subscription$.complete();
    }


}