import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AuthInterface } from '../../models/auth.model';
import { LeadService } from '../../services/leads.service';
import { AppState } from '../../states/app.state';

@Component({
    selector: 'app-create-lead',
    templateUrl: 'create-lead.component.html'
})

export class CreateLeadComponent implements OnInit {

    loading:boolean=false;
    formGroup:FormGroup;
    private userId:string='';
    private subscription$=new Subject();

    constructor(private state:AppState,private service:LeadService) {
        this.formGroup=new FormGroup(
            {
                createdBy:new FormControl('',Validators.required),
                name:new FormControl('',Validators.required),
                email:new FormControl('',Validators.required),
                description:new FormControl('',Validators.required),
                location:new FormControl('',Validators.required),
                businessType:new FormControl('',Validators.required),
                cnumber:new FormControl('',Validators.required),
                cperson:new FormControl('',Validators.required),
            }
        )
    }

    ngOnInit() {
        this.userId=this.state.auth$.value.user._id;
        this.formGroup.patchValue(
            {
                createdBy:this.userId
            }
        )
    }

    save():void{
        if(!this.formGroup.valid)return;
        this.loading=true;
        this.formGroup.disable;
        this.service.createLead(this.formGroup.value).pipe(takeUntil(this.subscription$)).subscribe((response:any)=>{
            this.loading=false;
            this.formGroup.reset();
            this.formGroup.enable;
            alert(response.message);
        })
    }


    ngOnDestroy(): void {
        this.subscription$.next();
        this.subscription$.complete();
    }


}