import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';
import { AuthTypes } from '../../models/common.model';
import { IUser } from '../../models/users.model';
import { LeadService } from '../../services/leads.service';
import { StageService } from '../../services/stages.service';
import { UserService } from '../../services/users.service';
import { AppState } from '../../states/app.state';

@Component({
    selector: 'lead-details',
    templateUrl: 'lead-details.component.html'
})

export class LeadDetailsComponent implements OnInit {

    authTypes=AuthTypes;
    formGroup:FormGroup;
    loading:'NOTES'|'ASSIGNED'|'STAGE'|''|any='';
    private selected:any;
    private subscription$=new Subject();


    constructor(public state:AppState,private fb:FormBuilder,
        private service:LeadService,private stageService:StageService,
        private userService:UserService,private router:Router) {
        this.formGroup=this.fb.group(
            {
                notes:new FormGroup(
                    {
                        notes:new FormControl('',Validators.required)
                    }
                ),
                assigned:new FormGroup(
                    {
                        assigned:new FormControl('',Validators.required)
                    }
                ),
                stage:new FormGroup(
                    {
                        stage:new FormControl('',Validators.required)
                    }
                )
            }
        )
    };

    ngOnInit() {
        this.selected=this.state.selected$.value;
        this.stageService.getstages();
        this.userService.loadUsers();
    };

    removeAssigned(id:string):void{
        const {_id,assigned}=this.selected;
        let filter=assigned.filter((a:IUser)=>a._id!==id);
        this.loading=id;
        const data={id:_id,data:{assigned:filter}};
        this.service.updateLead(data).pipe(takeUntil(this.subscription$)).subscribe((response:any)=>{
            this.loading='';
            alert(response.message);
            this.router.navigate(['home/leads']);
        })

    }

    removeStage(index:number):void{
        const {_id,stage}=this.selected;
        this.loading=`s${index}`;
        let filter=stage.filter((_,i)=>i!==index);
        const data={id:_id,data:{stage:filter}};
        this.service.updateLead(data).pipe(takeUntil(this.subscription$)).subscribe((response:any)=>{
            this.loading='';
            alert(response.message);
            this.router.navigate(['home/leads']);
        })
    }

    removeNote(index:number):void{
        const {_id,notes}=this.selected;
        this.loading=`n${index}`;
        let filter=notes.filter((_,i)=>i!==index);
        const data={id:_id,data:{notes:filter}};
        this.service.updateLead(data).pipe(takeUntil(this.subscription$)).subscribe((response:any)=>{
            this.loading='';
            alert(response.message);
            this.router.navigate(['home/leads']);
        })
    }

    assign():void{
        const form=this.formGroup.get('assigned');
        if(!form.valid)return;
        this.loading='ASSIGNED';
        const data={id:this.selected._id,data:{assigned:[...this.selected.assigned,form.value.assigned]}};
        this.service.updateLead(data).pipe(takeUntil(this.subscription$)).subscribe((response:any)=>{
            this.loading='';
            alert(response.message);
            form.reset();
            this.router.navigate(['home/leads']);
        })
    }

    saveStage():void{
        const form=this.formGroup.get('stage');
        if(!form.valid)return;
        this.loading='STAGE';
        let stage=[...this.selected.stage,form.value.stage];
        const data={id:this.selected._id,data:{stage}};
        this.service.updateLead(data).pipe(takeUntil(this.subscription$)).subscribe((response:any)=>{
            this.loading='';
            alert(response.message);
            form.reset();
            this.router.navigate(['home/leads']);
        })
    }

    addNotes():void{
        const form=this.formGroup.get('notes');
        if(!form.valid)return;
        this.loading='NOTES';
        let notes=[...this.selected.notes,form.value.notes];
        const data={id:this.selected._id,data:{notes}}
        this.service.updateLead(data).pipe(takeUntil(this.subscription$)).subscribe((response:any)=>{
            this.loading='';
            alert(response.message);
            form.reset();
            this.router.navigate(['home/leads']);
        })
    }


    edit():void{
        this.router.navigate(['home/leads/update']);
    }


    ngOnDestroy(): void {
        this.subscription$.next();
        this.subscription$.complete();
    }


}