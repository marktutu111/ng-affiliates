import { NbMenuItem } from '@nebular/theme';



export const MENU_ITEMS_AFF:NbMenuItem[]=[
  {
    title: 'Menu',
    group: true,
  },
  {
    title: 'Dashboard',
    icon: 'layout-outline',
    link:'/home/dashboard'
  },
  {
    title: 'Leads',
    icon: 'credit-card-outline',
    children:[
      {
        title:'Leads',
        icon:'list-outline',
        link:'/home/leads'
      },
      {
        title: 'Create Lead',
        icon: 'plus-square-outline',
        link:'/home/leads/new',
      }
    ]
  },
  {
    title: 'Settings',
    icon: 'settings-outline',
    link:'/home/settings'
  }
]


export const MENU_ITEMS: NbMenuItem[]=[
  {
    title: 'Menu',
    group: true,
  },
  {
    title: 'Dashboard',
    icon: 'layout-outline',
    link:'/home/dashboard'
  },
  {
    title: 'Affiliates',
    icon: 'people',
    link:'/home/affiliates'
  },
  {
    title: 'Leads',
    icon: 'credit-card-outline',
    children:[
      {
        title:'Leads',
        icon:'list-outline',
        link:'/home/leads'
      },
      {
        title: 'Create Lead',
        icon: 'plus-square-outline',
        link:'/home/leads/new',
      }
    ]
  },
  {
    title: 'Settings',
    icon: 'settings-outline',
    link:'/home/settings'
  },
  {
    title: 'Users',
    icon: 'credit-card-outline',
    link:'/home/users'
  },
];
