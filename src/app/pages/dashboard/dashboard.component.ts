import { Component, OnInit } from '@angular/core';
import {
    startOfDay,
    endOfDay,
    subDays,
    addDays,
    endOfMonth,
    isSameDay,
    isSameMonth,
    addHours,
  } from 'date-fns';
  import {
    CalendarEvent,
    CalendarEventAction,
    CalendarEventTimesChangedEvent,
    CalendarView,
  } from 'angular-calendar';
import { Subject } from 'rxjs';
import { AppState } from '../../states/app.state';
import { takeUntil } from 'rxjs/operators';
import { LeadInterface } from '../../models/leads.model';
import { LeadService } from '../../services/leads.service';
import { Router } from '@angular/router';

  const colors: any = {
    red: {
      primary: '#ad2121',
      secondary: '#FAE3E3',
    },
    blue: {
      primary: '#1e90ff',
      secondary: '#D1E8FF',
    },
    yellow: {
      primary: '#e3bc08',
      secondary: '#FDF1BA',
    },
  };

@Component({
    selector: 'app-dashboard',
    templateUrl: 'dashboard.component.html',
    styleUrls:['./dashboard.component.scss']
})

export class DashboardComponent implements OnInit {
    
    view: CalendarView=CalendarView.Month;
    CalendarView=CalendarView;
    viewDate:Date=new Date();

    actions:CalendarEventAction[]=[
        {
            label:'<i class="fas fa-fw fa-info-circle"></i>',
            a11yLabel:'View',
            onClick:({ event }:{ event:CalendarEvent }):void=>{
                this.state.setSelected(event.meta);
                this.router.navigate(['/home/leads/details'])
            }
        }
    ];

    refresh=new Subject<void>();
    private subscription$=new Subject();

    events:CalendarEvent[]=[];
    activeDayIsOpen:boolean=true;

  
    constructor(private state:AppState,private leadService:LeadService,private router:Router) {};


    dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
        if (isSameMonth(date, this.viewDate)) {
          if (
            (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
            events.length === 0
          ) {
            this.activeDayIsOpen = false;
          } else {
            this.activeDayIsOpen = true;
          }
          this.viewDate = date;
        }
      }
    
      eventTimesChanged({
          event,
          newStart,
          newEnd,
        }:CalendarEventTimesChangedEvent): void {
            this.events = this.events.map((iEvent) => {
                if (iEvent === event) {
                    return {
                        ...event,
                        start: newStart,
                        end: newEnd,
                    };
                }
            return iEvent;
        });
    }

    handleEvent(event:CalendarEvent):void{
        this.state.setSelected(event.meta);
        this.router.navigate(['/home/leads/details'])
    }
    

    ngOnInit() {
        this.leadService.loadLeads();
        this.state.leads$.pipe(takeUntil(this.subscription$))
            .subscribe((data:LeadInterface[])=>{
                this.events=data.map(d=>{
                    return {
                        start: startOfDay(new Date(d.createdAt)),
                        title:d.description,
                        color:colors.yellow,
                        actions:this.actions,
                        meta:d
                    }
                })
            })
    };


    setView(view:CalendarView):void {
        this.view=view
    }
    

    closeOpenMonthViewDay():void {
        this.activeDayIsOpen=false;
    }


    ngOnDestroy(): void {
        this.subscription$.next();
        this.subscription$.complete();
    }



}