import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AffiliateService } from '../../services/affiliates.service';
import { Regions } from '../../utils/regions';

@Component({
    selector: 'app-signup',
    templateUrl: 'signup.component.html'
})

export class SignupComponent implements OnInit {

    regions:string[]=Regions;
    formGroup:FormGroup;
    loading:boolean=false;
    private subscription$=new Subject();

    constructor(private service:AffiliateService,private router:Router) {
        this.formGroup=new FormGroup(
            {
                fname:new FormControl('',Validators.required),
                lname:new FormControl('',Validators.required),
                phone:new FormControl('',Validators.required),
                email:new FormControl('',[Validators.required,Validators.email]),
                profession:new FormControl('',Validators.required),
                organization:new FormControl('',Validators.required),
                city:new FormControl('',Validators.required),
                region:new FormControl('',Validators.required),
                gender:new FormControl('',Validators.required),
                password:new FormControl('',Validators.required),
                confirmPassword:new FormControl('',Validators.required)
            }
        )
    }


    ngOnInit() {};


    signup():void{
        const form=this.formGroup;
        if(!form.valid)return;
        if(form.value.password !== form.value.confirmPassword){
            return alert(
                'Password does not match'
            )
        }
        this.loading=true;
        this.formGroup.disable;
        let data=form.value;
        delete data.confirmPassword;
        this.service.add(data).pipe(takeUntil(this.subscription$)).subscribe((response:any)=>{
            this.loading=false;
            alert(response.message);
            this.formGroup.reset();
            this.formGroup.enable;
            this.router.navigate(['/auth/login']);
        })
    }





}