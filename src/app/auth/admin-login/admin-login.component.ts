import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AuthInterface } from '../../models/auth.model';
import { IUser } from '../../models/users.model';
import { UserService } from '../../services/users.service';
import { AppState } from '../../states/app.state';

@Component({
    selector: 'admin-login',
    templateUrl: 'admin-login.component.html'
})

export class AdminLoginComponent implements OnInit {

    formGroup:FormGroup;
    loading:boolean=false;
    private subscription$=new Subject();

    constructor(
        private service:UserService,private router:Router,
        private state:AppState) {
            this.formGroup=new FormGroup(
                {
                    email:new FormControl('',[Validators.required,Validators.email]),
                    password:new FormControl('',Validators.required)
                }
            )
    };

    ngOnInit() {};

    login():void{
        const form=this.formGroup;
        if(!form.valid)return;
        this.loading=true;
        this.service.login(form.value).pipe(takeUntil(this.subscription$)).subscribe((response:any)=>{
            this.loading=false;
            if(!response.success)return alert(response.message);
            const data:AuthInterface=response.data;
            this.state.setUser(data);
            if(!data.user.active){return this.router.navigate(['/auth/admin/reset'])};
            this.router.navigate(['../../home/dashboard']);

        })
    }



}