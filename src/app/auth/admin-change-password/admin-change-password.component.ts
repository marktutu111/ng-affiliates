import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { UserService } from '../../services/users.service';
import { AppState } from '../../states/app.state';

@Component({
    selector: 'admin-change-password',
    templateUrl: 'admin-change-password.component.html'
})

export class AdminChangePasswordComponent implements OnInit {

    loading:boolean=false;
    formGroup:FormGroup;
    private subscription$=new Subject();

    constructor(private state:AppState,private SUser:UserService,private router:Router) {
        this.formGroup=new FormGroup(
            {
                oldpassword:new FormControl('',Validators.required),
                newpassword:new FormControl('',Validators.required)
            }
        )
    };


    ngOnInit() {};


    reset():void{
        const form=this.formGroup;
        if(!form.valid)return;
        this.loading=true;
        const {user}=this.state.auth$.value;
        const data={id:user._id,...form.value};
        this.SUser.updatePassword(data).pipe(takeUntil(this.subscription$))
            .subscribe((response:any)=>{
                this.loading=false;
                alert(response.message);
                if(response.success){
                    this.router.navigate(['/auth/admin']);
                }
            })
    }


    ngOnDestroy(): void {
        this.subscription$.next();
        this.subscription$.complete();
    }




}