import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AffiliateService } from '../../services/affiliates.service';
import { AppState } from '../../states/app.state';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls:['./login.component.scss']
})
export class LoginComponent implements OnInit {

    formGroup:FormGroup;
    private subscription$=new Subject();
    loading:boolean=false;

    constructor(private router:Router,private service:AffiliateService,private state:AppState) {
        this.formGroup=new FormGroup(
            {
                email:new FormControl('',[Validators.required,Validators.email]),
                password:new FormControl('',Validators.required)
            }
        )
    };

    ngOnInit(): void {};


    login():void{
        const form=this.formGroup;
        if(!form.valid)return;
        this.loading=true;
        this.service.login(form.value).pipe(takeUntil(this.subscription$)).subscribe((response:any)=>{
            this.loading=false;
            if(!response.success)return alert(response.message);
            this.state.setUser(response.data);
            this.router.navigate(['../../home/dashboard']);
        })
    }


}
