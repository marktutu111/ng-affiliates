import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthenticationComponent } from './auth.component';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { ReactiveFormsModule } from '@angular/forms';
import { SignupComponent } from './signup/signup.component';
import { AffiliateService } from '../services/affiliates.service';
import { AdminChangePasswordComponent } from './admin-change-password/admin-change-password.component';
import { UserService } from '../services/users.service';
import { AuthGuard } from '../guards/auth.guard';
import { AdminLoginComponent } from './admin-login/admin-login.component';

const route:Routes=[
    {
        path:'',
        redirectTo:'login',
        pathMatch:'full'
    },
    {
        path:'',
        component:AuthenticationComponent,
        children:[
            {
                path:'login',
                component:LoginComponent
            },
            {
                path:'signup',
                component:SignupComponent
            },
            {
                path:'admin/reset',
                canActivate:[AuthGuard],
                component:AdminChangePasswordComponent
            },
            {
                path:'admin',
                component:AdminLoginComponent
            }
        ]
    }
]

@NgModule({
    declarations: [
        AuthenticationComponent,
        LoginComponent,
        SignupComponent,
        AdminChangePasswordComponent,
        AdminLoginComponent
    ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        RouterModule.forChild(route)
    ],
    exports: [],
    providers: [
        AffiliateService,
        UserService
    ],
})
export class AuthModule {}