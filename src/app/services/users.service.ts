import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { IUser } from '../models/users.model';
import { AppState } from '../states/app.state';
import { BASEURL } from '../utils/api';

@Injectable({providedIn: 'root'})
export class UserService {

    private subscription$=new Subject();
    constructor(private http:HttpClient,private state:AppState) {};

    adduser(data:IUser):Observable<any>{
        return this.http.post(`${BASEURL}/users/new`,data)
    }

    login(data:IUser):Observable<any>{
        return this.http.post(`${BASEURL}/users/login`,data)
    }

    update(data:{id:string,data:IUser|any}):Observable<any>{
        return this.http.put(`${BASEURL}/users/update`,data)
    }

    updatePassword(data:{id:string,oldpassword:string,newpassword:string}):Observable<any>{
        return this.http.put(`${BASEURL}/users/password/update`,data)
    }

    loadUsers():void{
        this.http.get(`${BASEURL}/users/get`)
            .pipe(takeUntil(this.subscription$))
            .subscribe((response:any)=>{
                if(response.success){
                    this.state.users$.next(
                        response.data
                    )
                }
            })
    }

    ngOnDestroy(): void {
        this.subscription$.next();
        this.subscription$.complete();
    }
    
}