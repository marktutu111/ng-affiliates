import { HttpClient } from '@angular/common/http';
import { Injectable, OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AffiliatesInterface } from '../models/affiliates.model';
import { AppState } from '../states/app.state';
import { BASEURL } from '../utils/api';

@Injectable({providedIn: 'root'})
export class AffiliateService implements OnDestroy {

    private subscription$=new Subject();

    constructor(private http:HttpClient,private state:AppState) {};

    add(data:AffiliatesInterface):Observable<any>{
        return this.http.post(`${BASEURL}/affiliates/new`,data);
    }

    login(data:AffiliatesInterface):Observable<any>{
        return this.http.post(`${BASEURL}/affiliates/login`,data);
    }

    updatePassword(data:any):Observable<any>{
        return this.http.put(`${BASEURL}/affiliates/password/update`,data);
    }

    update(data:any):Observable<any>{
        return this.http.put(`${BASEURL}/affiliates/update`,data);
    }

    getall():void{
        this.http.get(`${BASEURL}/affiliates/get`)
            .pipe(takeUntil(this.subscription$))
            .subscribe((response:any)=>{
                if(response.success){
                    this.state.setAffiliates(
                        response.data
                    )
                }
            })
    }

    ngOnDestroy(): void {
        this.subscription$.next();
        this.subscription$.complete();
    }
    

    
}