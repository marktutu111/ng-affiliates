import { HttpClient } from '@angular/common/http';
import { Injectable, OnDestroy } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { AuthTypes } from '../models/common.model';
import { LeadInterface, LeadModel } from '../models/leads.model';
import { AppState } from '../states/app.state';
import { BASEURL } from '../utils/api';

@Injectable({
    providedIn: 'root'
})
export class LeadService implements OnDestroy {

    private subscription$=new Subject();
    constructor(private http:HttpClient,private state:AppState){};

    createLead(data:LeadInterface):Observable<any>{
        return this.http.post(`${BASEURL}/leads/create`,data)
    }

    updateLead(data:any):Observable<any>{
        return this.http.put(`${BASEURL}/leads/update`,data)
    }

    loadLeads():void{
        let url:string=`${BASEURL}/leads/get`;
        const {type,user}=this.state.auth$.value;
        url=type===AuthTypes.AFFILIATE?`${url}/${user._id}`:url;
        this.http.get(url).pipe(takeUntil(this.subscription$))
            .subscribe((response:any)=>{
                if(response.success){
                    let data=response.data.map((d:any)=>new LeadModel(d));
                    this.state.leads$.next(
                        data
                    )
                }
            })
    }
    

    ngOnDestroy(): void {
        this.subscription$.next();
        this.subscription$.complete();
    }


}