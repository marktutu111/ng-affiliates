import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { StagesInterface } from '../models/stages.model';
import { AppState } from '../states/app.state';
import { BASEURL } from '../utils/api';

@Injectable({providedIn: 'root'})
export class StageService {

    private subscription$=new Subject();
    constructor(private http:HttpClient,private state:AppState) {}

    add(data:StagesInterface):Observable<any>{
        return this.http.post(`${BASEURL}/stages/add`,data)
    }

    remove(id:string):Observable<any>{
        return this.http.delete(`${BASEURL}/stages/delete/${id}`)
    }

    getstages():void{
        this.http.get(`${BASEURL}/stages/get`)
            .pipe(takeUntil(this.subscription$))
            .subscribe((response:any)=>{
                if(response.success){
                    this.state.stages$.next(
                        response.data
                    )
                }
            })
    }

    ngOnDestroy(): void {
        this.subscription$.next();
        this.subscription$.complete();
    }
    
}