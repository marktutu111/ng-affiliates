import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { StagesInterface } from '../models/stages.model';
import { AppState } from '../states/app.state';
import { BASEURL } from '../utils/api';

@Injectable({providedIn: 'root'})
export class AccountService {

    private subscription$=new Subject();
    constructor(private http:HttpClient,private state:AppState) {}

    add(data:StagesInterface):Observable<any>{
        return this.http.post(`${BASEURL}/accounts/add`,data)
    }

    remove(id:string):Observable<any>{
        return this.http.delete(`${BASEURL}/accounts/delete/${id}`)
    }

    accounts():void{
        this.http.get(`${BASEURL}/accounts/get`)
            .pipe(takeUntil(this.subscription$))
            .subscribe((response:any)=>{
                if(response.success){
                    this.state.accounts$.next(
                        response.data
                    )
                }
            })
    }

    ngOnDestroy(): void {
        this.subscription$.next();
        this.subscription$.complete();
    }
    
}