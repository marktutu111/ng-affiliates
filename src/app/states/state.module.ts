import { NgModule } from '@angular/core';
import { AppState } from './app.state';


@NgModule({
    providers: [AppState],
})
export class StateModule {};
